c3dl.addMainCallBack(canvasMain, "myCanvas");
c3dl.addModel("canape.dae");
c3dl.addModel("wall.dae");
c3dl.addModel("lamp.dae");
var scn;
var sofa;
var floor2;
var wall;
var wall1;
var lamp;

var diffuse = new c3dl.PositionalLight();

var demoMat = new c3dl.Material();
demoMat.setDiffuse([1,0.6,0.6]);

var lightMat = new c3dl.Material();
lightMat.setDiffuse([0.3,1.0,0.4]);

var wallMat = new c3dl.Material();
wallMat.setDiffuse([1,0.136,0.6]);

// The program main
function canvasMain(canvasName){

 // Create new c3dl.Scene object
 scn = new c3dl.Scene();
 scn.setCanvasTag(canvasName);

 // Create GL context
 renderer = new c3dl.WebGL();
 renderer.createRenderer(this);

 // Attach renderer to the scene
 scn.setRenderer(renderer);
 scn.setAmbientLight([1,1,1,0.1]);
 
 scn.init(canvasName);

 if(renderer.isReady() )
 {
 sofa = new c3dl.Collada();
 floor2 = new c3dl.Plane(25, 25);
 wall = new c3dl.Collada();
 wall1 = new c3dl.Collada();
 lamp = new c3dl.Collada();

 sofa.init("canape.dae");
 sofa.setTexture("sofa_1.png");
 sofa.setMaterial(demoMat);
 
 floor2.setTexture("floor_2.png");
 
 wall.init("wall.dae");
 wall.setTexture("wall_1.png");
 wall.setMaterial(demoMat);
 wall1.init("wall.dae");
 wall1.setTexture("wall_1.png");
 wall1.setMaterial(demoMat);
 
 lamp.init("lamp.dae");
 lamp.setTexture("sofa_4.png");
 lamp.setMaterial(lightMat);

 // Add the object to the scene
 scn.addObjectToScene(sofa);
 scn.addObjectToScene(floor2);
 scn.addObjectToScene(wall);
 scn.addObjectToScene(wall1);
 scn.addObjectToScene(lamp);

 // Create a camera
 var cam = new c3dl.FreeCamera();

 // Place the camera.
 // WebGL uses a right handed co-ordinate system.
 // move 200 to the right
 // move 300 up
 // move 500 units out
 //cam.setPosition(new Array(200.0, 300.0, 500.0));
 var camInitPos = new Array(0.0, -0.00, 0.0)
 camInitPos[0] += 15.0;
 camInitPos[1] += 10.0;
 camInitPos[2] += 0.0;
 cam.setPosition(camInitPos);

 cam.setLookAtPoint(new Array(sofa.getPosition()[0],sofa.getPosition()[1] + 3.5,sofa.getPosition()[2]));
 
 sofa.setPosition(new Array(sofa.getPosition()[0], sofa.getPosition()[1] + 1.5,sofa.getPosition()[2]));
 sofa.yaw(1.4);
 
 wall.setPosition(new Array(sofa.getPosition()[0]-7, sofa.getPosition()[1], sofa.getPosition()[2]));
 wall.scale(new Array(2,2,2));
 wall1.setPosition(new Array(sofa.getPosition()[0]+6, sofa.getPosition()[1], sofa.getPosition()[2]+6));
 wall1.scale(new Array(2,2,2));
 wall1.yaw(1.5);
 
 floor2.setPosition(new Array(floor2.getPosition()[0], floor2.getPosition()[1], floor2.getPosition()[2]-6));
 
 lamp.scale(new Array(0.1,0.1,0.1));
 lamp.setPosition(new Array(sofa.getPosition()[0]- 13.5 ,sofa.getPosition()[1] - 1.5,sofa.getPosition()[2] - 2.5));

 // Add the camera to the scene
 scn.setCamera(cam);
 
  diffuse.setName('diffuse');
  diffuse.setPosition(new Array(lamp.getPosition()[0],lamp.getPosition()[1] + 0.5,lamp.getPosition()[2]));
  diffuse.setDiffuse([1,1,1,1]);
  diffuse.setOn(true);
  scn.addLight(diffuse);
  
  var directional = new c3dl.DirectionalLight();
  directional.setName('dir');
  directional.setDirection([1,-1,0.5]);
  directional.setDiffuse([1,1,1,0.1]);
  directional.setOn(true);
  scn.addLight(directional);
 
  scn.setAmbientLight([0.8,0.8,0.8,0.8]);
  
  // Start the scene
  scn.setKeyboardCallback(up);
  scn.startScene();
 }
 
 function up(event){//a key is released
    var cam = scn.getCamera();
    if(event.shiftKey) {
        switch(event.keyCode) {//determine the key released
            case 65://a key
                cam.roll(-Math.PI * 0.025);//tilt to the left
                break;
            case 37://left arrow
                cam.yaw(Math.PI * 0.025);//yaw to the left
                break;
            case 68://d key
                cam.roll(Math.PI * 0.025);//tilt to the right
                break;
            case 39://right arrow
                cam.yaw(-Math.PI * 0.025);//yaw to the right
                break;
            case 83://s key
            case 40://down arrow
                cam.pitch(Math.PI * 0.025);//look down
            break;
            case 87://w key
            case 38://up arrow
                cam.pitch(-Math.PI * 0.025);//look up
            break;
        }
    }
    else {
        var pos = cam.getPosition();
        var mov = [0,0,0];
        switch(event.keyCode) {//determine the key released
            case 65://a key
            case 37://left arrow
                mov = c3dl.multiplyVector(cam.getLeft(),10,mov);//move the camera left
            break;
            case 68://d key
            case 39://right arrow
                mov = c3dl.multiplyVector(cam.getLeft(),-10,mov);//move the camera right
            break;
            case 83://s key
                mov = c3dl.multiplyVector(cam.getUp(),-10,mov);//move the camera down
            break;
            case 40://down arrow
                mov = c3dl.multiplyVector(cam.getDir(),-10,mov); //move the camera 'back' (towards the user)
            break;
            case 87://w key
                mov = c3dl.multiplyVector(cam.getUp(),10,mov); //move the camera up
            break;
            case 38://up arrow
                mov = c3dl.multiplyVector(cam.getDir(),10,mov);//move the camera 'forward' (into the scene)
            break;
        }
        cam.setPosition([pos[0]+mov[0],pos[1]+mov[1],pos[2]+mov[2]]);
    }
}
}
function changeTexture(number) {
	sofa.setTexture("sofa_" + number + ".png");
}
function switchLamp() {
	if(diffuse.isOn()) {
		diffuse.setOn(false);
		scn.setAmbientLight([0.2,0.2,0.2,0.2]);
	} else {
		diffuse.setOn(true);
		scn.setAmbientLight([0.8,0.8,0.8,0.8]);
	}
}
var counter = 0;
function rotateSofaFromButton(value) {
	if ((counter > -0.3) && value < 0 || (counter < 0.8 && value > 0)) {
		sofa.yaw(value);
		counter += value;
	}
}